# Sentinel One documentation

*******
Table des matières  
- [Sentinel One documentation](#sentinel-one-documentation)
  - [What is Sentinel One ?](#what-is-sentinel-one-)
  - [Financement](#financement)

*******

![Légende](./logo/sentinelone.png)

<div id='whatiss1'/>

## What is Sentinel One ?

SentinelOne est une entreprise américaine spécialisée en cybersécurité dont le siège social est situé à Mountain View en Californie2,3. La société a été fondée en 2013 par Tomer Weingarten, Almog Cohen et Ehud Shamir4. Tomer Weingarten est le CEO de l'entreprise2,3, Nicholas Warner est le COO5. Almog Cohen n'a plus de rôle actif dans l'entreprise6. La société compte environ 970 employés et des bureaux à Mountain View, Boston, Tokyo et Tel Aviv3,7,8. La plate-forme de l'entreprise utilise l'apprentissage automatique pour surveiller les ordinateurs personnels, les appareils IoT et les plateformes cloud3,9. Elle utilise un modèle heuristique implémenté dans son IA comportementale brevetée10. L'entreprise est certifiée AV-TEST11,12. 

<div id='whatiss1'/>

## Financement

En juin 2019, SentinelOne a levé 120 millions $ dans le cadre d'un tour de table de série D mené par Insight Partners10. La société a levé un financement supplémentaire de 200 millions $ de série E en février 2020 13 qui a porté sa valorisation à environ 1,1 milliard $3,2. En 2020, SentinelOne a clos un nouveau tour de table pour 267 millions $, portant sa valorisation totale à 3,1 milliard $14. Le 30 juin 2021, SentinelOne a réalisé une première offre au public de titres financiers sur le NYSE, levant 1,2 milliard $15. 

