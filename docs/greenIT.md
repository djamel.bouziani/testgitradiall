# GREEN IT pour les nuls 


*******
Le Green IT, en bref !

**Que signifie Green IT ?** Le Green IT, ou numérique responsable, désigne l’ensemble des pratiques ayant un impact réduit sur l’environnement.

**Pourquoi le Green IT ?** Pour réduire les impacts du numérique qui représente 4% des émissions de gaz à effet de serre dans le monde.

**Comment le Green IT peut participer à décarboner l'économie ?** Le Green IT vise la sobriété numérique en modifiant les usages.

**Comment faire du Green IT en 2023 ?** Il est possible de : verdir son hébergement web, prolonger la durée de vie de ses appareils ou encore utiliser un moteur de recherche écologique.

**Par quoi commencer pour lancer sa stratégie Green IT ?** Tout d'abord, il faut mesurer l’empreinte carbone de ses activités numériques.




Le but de cette doc est de donner les bonnes pratique pour économiser de l'energie et des ressource de notre planete en poursuivant le fonctionnement et la production chez **Radiall**


## I. Définition du Green IT

Green IT, informatique durable, numérique éco responsable, éco TIC, tous ces termes ont plus ou moins la même signification. On peut ainsi définir cette notion comme :

**« une démarche d’amélioration continue qui vise à réduire les impacts sur l’environnement, sociaux et économiques du numérique ».**

En gros le numérique responsable désigne l’ensemble des pratiques ayant un impact réduit sur l’environnement. Que ce soit dans notre société ou en entreprises, ces technologies de l’information et de la communication (TIC) permettent d’atteindre des objectifs du développement durable.


## II. Utiliser l'existant

Le numérique responsable s’organise en trois périmètres principaux : Green IT 1.0, Green IT 1.5, Green IT 2.0. Cette notation ne constitue pas une progression mais bien trois périmètres différents pour lesquels les interlocuteurs, le pouvoir de décision et le budget ne sont pas les mêmes. La plupart des organisations mènent des projets en parallèle au sein de ces trois périmètres.

## III. Prolonger la durée de vie


## IV. Les Quick-Win
